<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductStoreRequest;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{

    public function index()
    {
        $products = DB::table('products')->simplePaginate(9);
        return view('index', compact('products'));
    }

    public function create()
    {
        return view('products.productCreate');
    }

    public function store(ProductStoreRequest $request)
    {
        $validate = $request->validated();
        $product = new Product($validate);
        $product->save();
        return redirect('/')->with('status', __('Продукт успешно создан'));
    }

    public function show(string $id)
    {
        $product = Product::find($id);
        return view('products.productShow', compact('product'));
    }

    public function edit(string $id)
    {
        $product = Product::find($id);
        return view('products.productEdit', compact('product'));
    }

    public function update(ProductStoreRequest $request, string $id)
    {
        $validated = $request->validated();
        $product = Product::find($id);
        $product->update($validated);
        return redirect('/')->with('status', __('Продукт отредактирован'));
    }

    public function destroy(string $id)
    {
        $product = Product::find($id);
        $product->delete();
        return redirect('/')->with('status', __('Продукт удалён'));
    }
}
