<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'bail', 'min:2', 'max:100', 'string'],
            'description' => ['required', 'bail', 'min:5', 'max:800', 'string'],
            'price' => ['required', 'bail', 'min:1', 'max:10000', 'numeric']
        ];
    }
}
