@extends('layouts.app')
@section('content')
    <div class="container" style="margin-bottom: 100px">
        <div style="justify-content: right">
            <a href="{{route('products.create')}}" id="product-create-link">{{__('Создать продукт')}}</a>
        </div>
        <div>
            <h1 id="index-title">
                {{__('Список наших товаров')}}
            </h1>
        </div>
        <div class="row">
            @foreach($products as $product)
                <div class="col-lg-3 col-md-4 col-sm-12 cards">
                    <a class="product-name" href="{{route('products.show', ['product' => $product->id])}}" style="justify-content: center; padding-left: 60px">
                        {{$product->name}}
                    </a>
                    <div id="delete-icon">
                        <div>
                                <form method="post" action="{{route('products.destroy', ['product' => $product->id])}}">
                                    @method('DELETE')
                                    @csrf
                                    <button style="background-color: transparent; border: none; color: blue" id="delete-button" type="submit"> <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-earmark-minus" viewBox="0 0 16 16">
                                            <path d="M5.5 9a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H6a.5.5 0 0 1-.5-.5"/>
                                            <path d="M14 4.5V14a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h5.5zm-3 0A1.5 1.5 0 0 1 9.5 3V1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V4.5z"/>
                                        </svg></button>
                                </form>
                        </div>
                    </div>
                    <div id="edit-icon">
                        <a href="{{route('products.edit', ['product' => $product->id])}}">
                            <svg style="text-align: right" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                                <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.5.5 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11z"/>
                            </svg>
                        </a>
                    </div>
                    <br>
                    <p class="product-description">{{$product->description}}</p><br>
                    <p class="product-price"><strong>{{$product->price}} {{__('сом')}}</strong></p>
                </div>
            @endforeach
        </div>
        {{$products->links()}}
    </div>
@endsection
