@extends('layouts.app')
@section('content')
    <div id="form-create-container" class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div>
            <h1>{{__('Создание продукта')}}</h1>
        </div>
        <div>
            <form method="POST" action="{{route('products.store')}}">
                @method('POST')
                @csrf
                <div>
                    <p>{{__('Название товара')}}</p><br>
                    <input name="{{__('name')}}" required placeholder="{{__('Введите название товара')}}" value="{{old('name')}}">
                </div>
                <div>
                    <p>{{__('Описане товара')}}</p><br>
                    <textarea placeholder="{{__('Описание товара')}}" name="description" required>{{old('description')}}</textarea>
                </div>
                <div>
                    <p>{{__('Цена товара')}}</p><br>
                    <input type="number" name="price" placeholder="{{__('Введите цену товара')}}" required value="{{old('price')}}" step="0.01">
                </div>
                <button id="create-button" type="submit">{{__('Создать товар')}}</button>
            </form>
        </div>
    </div>
@endsection
