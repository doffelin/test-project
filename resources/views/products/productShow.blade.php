@extends('layouts.app')
@section('content')
    <div class="container">
        <div>
            <h1 id="product-show-title">{{__('Страница товара')}}: {{$product->name}}</h1>
        </div>
        <div>
            <h2 id="product-description-title">{{__('Описание')}}:</h2>
        </div>
        <div>
            <p id="product-description-body">{{$product->description}}</p>
        </div>
        <div>
            <h2 id="product-price-title">{{__('Цена товара')}}:</h2><br>
            <p id="product-price">{{$product->price}} сом</p>
        </div>
    </div>
@endsection
