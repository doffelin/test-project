@extends('layouts.app')
@section('content')
    <div class="container">
        <div>
            <h1 id="about-us-title">
               {{__('О нас')}}
            </h1>
        </div>
        <div>
            <p id="about-us-body">
                Чтобы показать масштаб компании, следует добавить коллективное фото работников (если команда действительно велика). Фотография, сделанная в неофициальной атмосфере, — это даже плюс. Большое количество людей, объединенных общей деятельностью, улыбающиеся лица, красноречивая подпись — все это на странице о Ceros, которая позиционирует себя как первая компания, специалисты которой начали работать отдаленно: «Черт возьми. Нас около 400. И мы продолжаем расти. Интенсивно. В 15 странах».
            </p>
        </div>
        <div>
            <h2 id="contact-title">
                {{__('Контакты')}}
            </h2>
        </div>
        <div>
            <p id="contact-phone">
                {{__('Телефон')}}: +996 321 2312 3211
            </p><br>
            <p id="contact-email">
                {{__('Адрес электронной почты')}}: SuperPuper@mail.com
            </p>
        </div>
    </div>
@endsection
