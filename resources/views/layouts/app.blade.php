<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
    <!-- Fonts -->
    <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css">
    @vite(['resources/js/app.js', 'resources/sass/app.scss'])
</head>
<body class="antialiased">
<nav style="color: skyblue" class="navbar navbar-expand-lg bg-body-tertiary">
    <div style="background-color: skyblue" class="container-fluid">
        <a style="margin-left: 40px; font-size: 30px" class="navbar-brand" href="{{route('index')}}">Main Page</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02"
                aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a style="margin-left: 1300px" class="navbar-brand" href="{{route('aboutUs')}}">О нас</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02"
                aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02"
                aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        </div>
    </div>
</nav>
<div class="container-fluid">
    @if(session('status'))
        <div class="row">
            <div class="col">
                <div class="alert alert-primary" role="alert">
                    {{session('status')}}
                </div>
            </div>
        </div>
@endif
        </div>
@yield('content')
<footer id="footer">
        <h3 id="copyright">Copyright 2024</h3>
</footer>
</body>
</html>
