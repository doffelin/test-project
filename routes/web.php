<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [\App\Http\Controllers\ProductController::class, 'index'])->name('index');
Route::get('/about', [\App\Http\Controllers\AboutUsController::class, 'index'])->name('aboutUs');

Route::resource('products', \App\Http\Controllers\ProductController::class)->except('index');
