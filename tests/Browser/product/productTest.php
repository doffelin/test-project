<?php

namespace Tests\Browser\product;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class productTest extends DuskTestCase
{
    /**
     * @group productTest
     * Тест на отображение основной страницы
     */
    public function testSeeMainPage(): void
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')->waitForText(__('Список наших товаров'));
        });
    }
}
